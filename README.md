## Website Monitoring and Recovery

### Technologies used:
Python, AWS EC2, Docker, Linux

### Project Description:
 
1. Create a EC2 instance server on a AWS. 

2. Install Docker and run a Nginx website as a container on the EC2 server instance.

3. Write a Python script that regularly monitors the website by accessing it and validating the HTTP response. The Python script
   
   * sends an email notification when the website is down,
   * automatically restarts the application container when the application is down, and
   * automatically restarts the server and then the application container when the server is inaccessible.

### Usage Instructions:

#### Step 1: Create an EC2 server instance running Amazon Linux 2 OS on AWS:

![image](images/web-server.png)

#### Step 2: Install Docker on the EC2 Instance using the following bash script:

```
#!/bin/bash

yum update -y && yum install -y docker
                
systemctl start docker
systemctl enable docker
                
usermod -aG docker ec2-user
```

#### Step 3: Invoke Nginx web server as a docker container on the EC2 instance with the following command:

```
docker run -p 8080:80 nginx
```

#### Step 4: Update the security group of the EC2 instance to allow all traffic to the web server via port 8080

![image](images/website.png)

#### Step 5: Insert the following list of key pair values into an .env file under the root directory to set environment variables required by the Python script:

```
REGION_NAME=<xxx>

EMAIL_USER=<xxx>
EMAIL_APP_PASS=<xxx>

RECEIVER_EMAIL_ADDRESS=<xxx>

WEB_USER=<xxx>
KEY_NAME=<xxx>

NGINX_WEB_TAG=<xxx>

NGINX_WEB_PORT=<xxx>
NGINX_CONTAINER_ID=<xxx>
```

#### Step 6: Install the Python libraries of boto3, schedule, paramiko, requests, python-dotenv via:

```
pip install -r requirements.txt
```

#### Step 7: Monitor the website by accessing it and validating the HTTP response:

![image](images/monitoring-1.png)

#### Step 8: Send email notifications when the application container returns a status code other than 200 or the connection to the server has timed out:

![image](images/email-1.png)

![image](images/email-2.png)

#### Step 9: Automatically restart the application container or the server along with the application container when the website is down: 

![image](images/monitoring-2.png)

![image](images/monitoring-3.png)