import requests
import smtplib
import os
import paramiko
import boto3
import time
import schedule
from dotenv import load_dotenv

load_dotenv()

REGION_NAME = os.getenv("REGION_NAME")

EMAIL_USER = os.getenv("EMAIL_USER")
EMAIL_APP_PASS = os.getenv("EMAIL_APP_PASS")

RECEIVER_EMAIL_ADDRESS = os.getenv("RECEIVER_EMAIL_ADDRESS")

WEB_USER = os.getenv("WEB_USER")
KEY_NAME = os.getenv("KEY_NAME")

NGINX_WEB_TAG = os.getenv("NGINX_WEB_TAG")

NGINX_WEB_PORT = os.getenv("NGINX_WEB_PORT")
NGINX_CONTAINER_ID = os.getenv("NGINX_CONTAINER_ID")


ec2_client = boto3.client("ec2", region_name=REGION_NAME)


def check_instance_state_and_status(instance_id):
    response_status = ec2_client.describe_instance_status(
        InstanceIds=[
            instance_id,
        ],
        IncludeAllInstances=True,
    )

    state_status = []

    statutes = response_status["InstanceStatuses"]

    for status in statutes:
        ins_status = status["InstanceStatus"]["Status"]
        sys_status = status["SystemStatus"]["Status"]
        state = status["InstanceState"]["Name"]

        state_status.append(state)
        state_status.append(ins_status)
        state_status.append(sys_status)

    return state_status


def check_public_ips_ids():
    response_inst = ec2_client.describe_instances(
        Filters=[
            {
                "Name": "tag:Name",
                "Values": [
                    NGINX_WEB_TAG,
                ],
            },
        ]
    )

    public_ips_ids = []

    reservations = response_inst["Reservations"]

    for reservation in reservations:
        instances = reservation["Instances"]

        for instance in instances:
            try:
                public_ips_ids.append(instance["PublicIpAddress"])
            except KeyError:
                public_ips_ids.append(instance["PrivateIpAddress"])
            public_ips_ids.append(instance["InstanceId"])

    return public_ips_ids


def send_notification(body):
    print("Sending a notification email!")

    error_msg = f"Subject: WEBSITE DOWN\n{body}"

    with smtplib.SMTP("smtp.gmail.com", 587) as smtp:
        smtp.starttls()
        smtp.ehlo()
        smtp.login(EMAIL_USER, EMAIL_APP_PASS)
        smtp.sendmail(EMAIL_USER, [RECEIVER_EMAIL_ADDRESS], error_msg)


def container_restart(server_public_ip):
    print("Restarting the application..")

    ssh = paramiko.SSHClient()

    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

    ssh.connect(server_public_ip, username=WEB_USER, key_filename=KEY_NAME)

    stdin, stdout, stderr = ssh.exec_command(f"docker restart {NGINX_CONTAINER_ID}")

    print(stdout.readlines())

    ssh.close()

    print("Application restarted.")


def web_server_restart(server_id):
    state = check_instance_state_and_status(server_id)[0]

    if state != "stopped":
        print("Stopping web server..")
        ec2_client.stop_instances(InstanceIds=[server_id])

        state = check_instance_state_and_status(server_id)[0]

        while state != "stopped":
            state = check_instance_state_and_status(server_id)[0]
            time.sleep(15)

        print("Web server stopped.")

    print("Restarting web server..")
    ec2_client.start_instances(InstanceIds=[server_id])

    print("Web server restarted.")

    state = check_instance_state_and_status(server_id)[0]

    ins_status = check_instance_state_and_status(server_id)[1]
    sys_status = check_instance_state_and_status(server_id)[2]

    while state != "running" or ins_status != "ok" or sys_status != "ok":
        print("Checking the state and statuses of the web server...")
        time.sleep(30)

        state = check_instance_state_and_status(server_id)[0]
        ins_status = check_instance_state_and_status(server_id)[1]
        sys_status = check_instance_state_and_status(server_id)[2]

    print("Web server is running with OK statuses.")


def monitor_website():
    server_public_ip = check_public_ips_ids()[0]
    server_id = check_public_ips_ids()[1]

    server_url = f"http://{server_public_ip}:{int(NGINX_WEB_PORT)}"

    try:
        http_response = requests.get(server_url, timeout=5)
        status_code = http_response.status_code

        if status_code == 200:
            print("Application is running successfully!")
        else:
            print(f"Application is down with status code {status_code}.")

            msg_body = (
                f"Application returned status code {status_code}.\n"
                f"Fix the issue ASAP!\n "
                f"Restart the app and/or the server!"
            )

            # Send e-mail to the operations team
            send_notification(msg_body)

            # Restart the application
            container_restart(server_public_ip)
    except requests.exceptions.ConnectionError as ex:
        print(f"Connection error happened!\n {ex}")

        msg_body = (
            "Application is not accessible at all.\n"
            "Connection error happened.\n"
            "Fix the issue ASAP!\n "
            "Restart the app and/or the server!"
        )

        # Send e-mail to the operations team
        send_notification(msg_body)

        # Restart EC2 Server
        web_server_restart(server_id)

        # Restart the application
        server_public_ip = check_public_ips_ids()[0]

        container_restart(server_public_ip)


schedule.every(15).seconds.do(monitor_website)

while True:
    schedule.run_pending()
